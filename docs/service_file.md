# README

Readme file for the systemd service file for the windsonic python scripts.


In the present for the script is written to run on dietpi running on raspberry pi.
As such, the user is root and has all privileges. Therefore, the script can be executed without any special elevations or privileges.

The scripts reside in the _/root/windsonic_rpi/scripts_ folder.

The service file just specifies the working directory and the script to run.
