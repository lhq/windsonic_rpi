# README

A brief set of instructions, dependancies and troubleshooting for the installation of quectel EC25-EU on a debian based distro running on a 
raspberry pi. Applies for dietpi, raspbian.

## Source for the actual driver building instructions.
https://docs.sixfab.com/page/qmi-interface-internet-connection-setup-using-sixfab-shield-hat



## Install Script Location

https://github.com/sixfab/Sixfab_QMI_Installer


## Prerequisites
The latest kernel version. 
Make sure to run

```bash
sudo apt-get update && sudo apt-get upgrade
```
before proceeding further

## Dependancies

make, ifconfig, raspberrypi kernel headers for the specific kernel version

make can be installed by running 

```bash
sudo apt-get install build-essential
```

ifconfig can be installed by running

```bash
sudo apt-get install net-tools
```

raspberry pi kernel headers can be installed by running

```bash
sudo apt-get install raspberrypi-kernel-headers
```

## Usage
After installation, the final binary to run the modem connect is located at
_/opt/qmi_files/quectel-CM/_ . The binary is _quectel-CM_.

To connect to a network simply invoke **quectel-CM -s _APN_**

## Diet Pi specific

To autoconnect the modem on power up, the following changes are made to the 
custom script present at _/var/lib/dietpi/dietpi-autostart/_

Add the following line at the end of the file.

```bash
sudo /opt/qmi_files/quectel-CM/quectel-CM -s data.mono
```

This takes a while on powerup as the diet pi tries to synchronize network but cannot until a network connection is present. This retries happens for a 60 retry count. And the custom script is run only after the time synchronization times out.


_The auto_reconnect script present on the repo does not seem to work on the dietpi. On a regular debian system, it might work. The idea is to have the above command run on system bootup._
