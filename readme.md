## README

----

[[_TOC_]]

----

Python scripts for interfacing with the Gill instruments windsonic sensor (Option 1) connected to a raspberry pi.

## Hardware

1. Raspberry Pi
2. Gill Windsonic ( Option 1 )
3. MAX232 level shifter
4. Qualcomm MSM Interface

### Prerequisites

The sensor itself is powered from a 12V power supply. This can be a DC-DC or an offline AC-DC supply. 

The wiring for the wind sensor has to be done as a typical connection circuit shown in the MAX232 datasheet on page 10, figure 6. Pins 9,10,11,12 go to the 3.3V logic level side and the pins  7,8,13,14 go to the sensor side. 

_However, the supply voltage for the MAX232 is 3.3V powered from the raspberry pin 1 rather than 5V._

_The sensor V-, GND, MAX232 Gnd, Pi Gnd must all be commoned to have a common reference ground._

**DO NOT power the MAX232 from 5V. PI gpio pins are NOT 5V tolerant**

## Software
First flash an OS into a SD card and connect it to the same WiFi network that the localhost is connected to and enable ssh interface with ```sudo raspi-config```.

The recommended system is Raspbian. The source code has been tested on the OS with following version details:
* Distributor ID: Raspbian
* Description:    Raspbian GNU/Linux 10 (buster)
* Release:        10
* Codename:       buster

Type in the following command to see version details of your OS
```lsb_release -a```

### Prerequisites

1. Localhost:
	* Ansible (remember to create the .hosts file locally)
	* [windsonic_rpi](https://bitbucket.org/lhq/windsonic_rpi/src/master/) repo installed
2. Raspberry Pi:
	* UART and Serial port enabled (use ```sudo raspi-config```)
	* Python 3
	* python-3 pip
	* Zerotier 
	* Watchdog
	* make (further instructions are given in docs/ modem_instructions.md)
	* ifconfig (further instructions are given in docs/ modem_instructions.md)

## Installation
**Windsonic source code**
1. First of all using deploy the source code 
``` bash
ansible-playbook deploy.yml -i .hosts
```
2. Login into the raspberry Pi with SSH
``` bash
 ssh pi@<ip adress of the Raspberry Pi>
```
3. Install required libraries using pip3
``` bash
pip3 install -r ./windsonic_rpi/scripts/requirements.txt
```
4. Then make sure that the script work by running
``` bash
python3 ./windsonic_rpi/scripts/main.py
```

_You might need to identify the correct serial port and modify it accordingly in the uart.py file. With the modem installed, yhe correct port will be /dev/ttyAMA0. If the modem is not installed then the script will work with /dev/ttyS0 port also._

You should see the sensor data displayed on screen and a post request with the response on the screen once every 10 seconds.

Now quit the script, and install the service files to kickstart the service.

**Setup of the daemon service**
1. Reload the systemd daemon.
``` bash
sudo systemctl daemon-reload 
``` 
2. Check status of windsonic daemon
```bash
sudo systemctl status windsonic.service
```
3. Start the windsonic daemon
```bash
sudo systemctl start windsonic.service
```

4. Check status of windsonic daemon, to verify that the service is actually running and has started.
```bash
sudo systemctl status windsonic.service
```
5. If the above step completes successfully, then finally enable the service by running
```bash
sudo systemctl enable windsonic.service
```
This should autostart the service on power up. 

**Watchdog installation**
Follow the instructions at

https://www.supertechcrew.com/watchdog-keeping-system-always-running/

to setup the watchdog as per requirements. The internal watchdog can be used to monitor system activity, network activity, interface activity and run a varierty of retries, repair scripts to wake the system in event of a watchdog timeout. For the basic purposes the samples configurations of the watchdog are given below.

/etc/watchdog.conf

```bash
#ping                   = 172.31.14.1
#ping                   = 172.26.1.255
#interface              = eth0
#file                   = /var/log/messages
#change                 = 1407

# Uncomment to enable test. Setting one of these values to '0' disables it.
# These values will hopefully never reboot your machine during normal use
# (if your machine is really hung, the loadavg will go much higher than 25)
max-load-1              = 24
#max-load-5             = 18
#max-load-15            = 12

# Note that this is the number of pages!
# To get the real size, check how large the pagesize is on your machine.
min-memory              = 1
#allocatable-memory     = 1

#repair-binary          = /usr/sbin/repair
#repair-timeout         = 60
#test-binary            =
#test-timeout           = 60

# The retry-timeout and repair limit are used to handle errors in a more robust
# manner. Errors must persist for longer than retry-timeout to action a repair
# or reboot, and if repair-maximum attempts are made without the test passing a
# reboot is initiated anyway.
#retry-timeout          = 60
#repair-maximum         = 1

watchdog-device = /dev/watchdog

# Defaults compiled into the binary
#temperature-sensor     =
#max-temperature        = 90

# Defaults compiled into the binary
#admin                  = root
#interval               = 1
#logtick                = 1
#log-dir                = /var/log/watchdog

# This greatly decreases the chance that watchdog won't be scheduled before
# your machine is really loaded
realtime                = yes
priority                = 1

# Check if rsyslogd is still running by enabling the following line
#pidfile                = /var/run/rsyslogd.pid

watchdog-timeout = 15

```
and cat /etc/default/watchdog

```bash
# Start watchdog at boot time? 0 or 1
run_watchdog=1
# Start wd_keepalive after stopping watchdog? 0 or 1
run_wd_keepalive=1
# Load module before starting watchdog
watchdog_module="none"
# Specify additional watchdog options here (see manpage).
```

The above defaults can be used as a template to further customize and configure the watchdog to suit a specific trigger to reboot and/or to run a repair script.

**Sixfab_QMI drivers installation**

Useful links:
- [Installation guide from Sixfab](https://docs.sixfab.com/page/qmi-interface-internet-connection-setup-using-sixfab-shield-hat)
- [Github repo of the drivers](https://github.com/sixfab/Sixfab_QMI_Installer)
1. Update the OS 
```sudo apt update && sudo apt upgrade -y```

2. Install kernel headers
```sudo apt-get install raspberrypi-kernel-headers```

3. Download *qmi_install.sh* shell script
```wget https://raw.githubusercontent.com/sixfab/Sixfab_RPi_3G-4G-LTE_Base_Shield/master/tutorials/QMI_tutorial/qmi_install.sh```

4. Change permissions to the file
```sudo chmod +x qmi_install.sh```

5. Run the shell script
```sudo ./qmi_install.sh```

6. ```cd files/quectel-CM```

7. ```sudo ./quectel-CM -s data.mono```

8. Wait for the reboot

9. Setup autoconnect on boot. Install shell script with 
```wget https://raw.githubusercontent.com/sixfab/Sixfab_RPi_3G-4G-LTE_Base_Shield/master/tutorials/QMI_tutorial/install_auto_connect.sh```

10. Change permissions to the file
```sudo chmod +x install_auto_connect.sh```

11. Run the shell script, when asked for APN type in "data.mono"
```sudo ./install_auto_connect.sh```

**ZeroTier**

1. Install ZaeroTier
```
curl -s 'https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg' | gpg --import && \
if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi
```
2. Join the Leapcraft network 

**Disable WiFi interface**

1. Edit config file
```sudo nano /boot/config.txt```
2. in [ALL] section, in the bottom of the file add
```dtoverlay=disable-wifi```





