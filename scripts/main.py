import datetime
import json
import logging
from logging.handlers import TimedRotatingFileHandler
import requests
import sys
import time

from uart import UART

ingestion_url = "https://ingestion-dev.cphsense.com"

logging.basicConfig(level=logging.INFO)
logname = "/home/pi/windsonic_rpi/logs/windsonic.log"
log_format = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s','%Y-%m-%dT%H:%M:%S%z')

logger = logging.getLogger('windsonic_logger')
# Remove the default top-level console logging
logger.propagate = False

fl = TimedRotatingFileHandler(
	logname, 
	when='midnight',
	encoding='utf-8', 
	backupCount=30, 
	utc=True
)
fl.setFormatter(log_format)
fl.setLevel(logging.DEBUG)
logger.addHandler(fl)

stdl = logging.StreamHandler(sys.stdout)
stdl.setFormatter(log_format)
stdl.setLevel(logging.INFO)
logger.addHandler(stdl)

# Read the eth0 MAC ID to use as device ID for the posts
with open("/sys/class/net/eth0/address", 'r') as f:
	PHYSICAL_ID = f.read().strip().upper()
assert PHYSICAL_ID is not None

logger.info("STARTING SERVICE")
logger.info(f"Device ID: {PHYSICAL_ID}")

uart = UART()

while True:
	logger.info("Attempting to read")
	wind_parameters = uart.read()
	
	if wind_parameters is None:
		logger.warning("wind_parameters returned as None")
		continue

	logger.info("Parsed Sensor Data: ")
	logger.info(wind_parameters)

	# Create top level json object and nested objects
	json_root = {}
	json_payload = {}
	json_meta = {}

	# Standard template for the payload.
	# To Do: change ID to read the MAC ID of the host.
	json_root["app"] = "ambinode"
	json_root["id"] = PHYSICAL_ID
	json_root["type"] = "report"

	# Read the current system time and present in ISO8601 format. 
	# Discard the microseconds and add the trailing UTC timezone information
	json_root["time"] = (
		datetime.datetime.utcnow()
		.replace(microsecond=0, tzinfo=datetime.timezone.utc)
		.isoformat()
	)

	json_payload["windspeed"] = wind_parameters[2]
	json_payload["units"] = wind_parameters[3]
	json_payload["wind_direction"] = wind_parameters[1]

	json_meta["Sensor_Status"] = wind_parameters[4]


	json_root["payload"] = json_payload
	json_root["meta"] = json_meta

	payload = json.dumps(json_root, indent=4)

	logger.info(f"Payload: \r\n {payload}")
	logger.info("Making a  POST request to ingestion")

	try:
		post_result = requests.post(
				ingestion_url,
				payload,
				headers={"Content-Type": "application/json"},
				timeout=10
		)
		logger.info("POST request successful")
	except requests.exceptions.ConnectionError as e:
		logger.error("POST Request failed")
		logger.error(e)
		continue
	except requests.exceptions.Timeout:
		logger.error("HTTP request timeout out")
		continue

	logger.info(f"Response: {post_result.text}")

	logger.info("Sleeping for 10 seconds")
	time.sleep(10)
