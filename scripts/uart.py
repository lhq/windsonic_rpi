#!/usr/bin/env python3

import logging
import time

import serial


logger = logging.getLogger('windsonic_logger')


class UART:
	"""
	Base UART class for sensor Pi communication. Has two methods - read and close. The uart port in the constructor has to adapted to the host being used.
	"""
	def __init__(self):

		logger.info("Initializing serial connection")
		self.serial = serial.Serial(
			port="/dev/ttyS0",
			baudrate=9600,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE,
			bytesize=serial.EIGHTBITS,
			timeout=10
		)

	def close(self):
		"""
		Close the serial port.
		"""
		logger.info("Closing serial connection")
		self.serial.close()

	def read(self):
		"""
		Reads from the windsonic sensor and parses the sensor string.

		Parameters
		----------
		None

		Returns
		-------
		res: list
			A list of the windspeed parameters wind direction, sind speed, units, sensor status, checksum in that order. The sensor returns the output in polar format by default.
			
			Wind Direction : indicated in degrees, can vary from 0 - 359.

			Wind Speed : Indicates the speed of wind. Units are the next item in the list

			Units : metres per second by default. It is identified by the letter 'M'

			Status : Status of the sensor. Returns '00' for correct operation. A list of error codes are given in section 11.5 in the user manual.

			Checksum : Exclusive OR of the bytes between ( and not including) the <STX> and <ETX> ascii characters, to ensure data correctness. unused for now.

		Notes
		-----
		Look at the user manual in the docs folder ( pg 27 ) for more information on the output format.

		The sample output from the sensor is of the form 
		<STX>Q, 229, 002.74, M, 00, <ETX> 16

		The wind direction is usually returned as an empty string if the measurement is too low or if there is no windspeed.

		"""
		max_retries = 5

		for i in range(max_retries):
			logger.info(f"Read attempt #{i+1}")

			if self.serial.in_waiting > 0:
				logger.info("Data available, reading a line")
				received = self.serial.readline()
				logging.info("Data read: {received}")
				try:
					raw_data = received.decode('utf-8','ignore').split(",")

					if len(raw_data) < 6:
						logger.error("Incorrect number of elements")
						logger.error("Received Data: ")
						logger.error(raw_data)
						continue
					else:
						raw_data = raw_data[-6:]
						try:
							# parse the value and save back to raw_data
							raw_data[1] = int(raw_data[1])
						except ValueError as e:
							# if we cannot parse it, check if it's empty
							if raw_data[1] == "":
								# if an empty string, consider it a missing value
								raw_data[1] = None
							else:
								# in any other case, consider it broken
								logging.error(f"Unable to parse value: {raw_data[1]}")
								continue
						try:
							raw_data[2] = float(raw_data[2])
						except ValueError:
							logging.error(f"Unable to parse value: {raw_data[2]}")
							continue
						try:
							assert raw_data[3] == 'M'
							assert raw_data[4] == "00"
						except AssertionError:
							logging.error("Raw data incorrect")
							logging.error(raw_data)
							continue
						logger.info("Raw Sensor data:")
						logger.info(raw_data)
						return raw_data

				except UnicodeDecodeError as e:
					logger.warning(f"Cannot decode: {e}")
					continue
				except Exception as e:
					logger.warning("Received data is not valid.")
					logger.exception(e)
					continue
			else:
				logger.error("No data in the buffer")
				time.sleep(.2)
		logging.error("Max number of retries (5) exceeded.")
